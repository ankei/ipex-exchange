package ai.turbochain.ipex.exception;

/**
 * @author jack
 * @date 2018年03月16日
 */
public class GeeTestException extends Exception {
    public GeeTestException(String msg) {
        super(msg);
    }
}
