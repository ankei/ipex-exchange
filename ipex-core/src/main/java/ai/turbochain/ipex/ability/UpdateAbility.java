package ai.turbochain.ipex.ability;

/**
 * @author jack
 * @Title: 
 * @Description:
 * @date 2020/4/2417:36
 */
public interface UpdateAbility<T> {
    T transformation(T t);
}
