package ai.turbochain.ipex.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMemberApplicationConfig is a Querydsl query type for MemberApplicationConfig
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMemberApplicationConfig extends EntityPathBase<MemberApplicationConfig> {

    private static final long serialVersionUID = -1906973220L;

    public static final QMemberApplicationConfig memberApplicationConfig = new QMemberApplicationConfig("memberApplicationConfig");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> promotionOn = createEnum("promotionOn", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> rechargeCoinOn = createEnum("rechargeCoinOn", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> transactionOn = createEnum("transactionOn", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> withdrawCoinOn = createEnum("withdrawCoinOn", ai.turbochain.ipex.constant.BooleanEnum.class);

    public QMemberApplicationConfig(String variable) {
        super(MemberApplicationConfig.class, forVariable(variable));
    }

    public QMemberApplicationConfig(Path<? extends MemberApplicationConfig> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMemberApplicationConfig(PathMetadata metadata) {
        super(MemberApplicationConfig.class, metadata);
    }

}

