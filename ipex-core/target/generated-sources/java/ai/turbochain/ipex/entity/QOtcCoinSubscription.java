package ai.turbochain.ipex.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QOtcCoinSubscription is a Querydsl query type for OtcCoinSubscription
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOtcCoinSubscription extends EntityPathBase<OtcCoinSubscription> {

    private static final long serialVersionUID = -1241144272L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOtcCoinSubscription otcCoinSubscription = new QOtcCoinSubscription("otcCoinSubscription");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> origin = createNumber("origin", Integer.class);

    public final QOtcCoin otcCoin;

    public final NumberPath<Integer> sort = createNumber("sort", Integer.class);

    public QOtcCoinSubscription(String variable) {
        this(OtcCoinSubscription.class, forVariable(variable), INITS);
    }

    public QOtcCoinSubscription(Path<? extends OtcCoinSubscription> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QOtcCoinSubscription(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QOtcCoinSubscription(PathMetadata metadata, PathInits inits) {
        this(OtcCoinSubscription.class, metadata, inits);
    }

    public QOtcCoinSubscription(Class<? extends OtcCoinSubscription> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.otcCoin = inits.isInitialized("otcCoin") ? new QOtcCoin(forProperty("otcCoin")) : null;
    }

}

