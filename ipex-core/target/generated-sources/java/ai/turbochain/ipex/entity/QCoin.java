package ai.turbochain.ipex.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCoin is a Querydsl query type for Coin
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCoin extends EntityPathBase<Coin> {

    private static final long serialVersionUID = 1844877421L;

    public static final QCoin coin = new QCoin("coin");

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> canAutoWithdraw = createEnum("canAutoWithdraw", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> canRecharge = createEnum("canRecharge", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> canTransfer = createEnum("canTransfer", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> canWithdraw = createEnum("canWithdraw", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final StringPath chainName = createString("chainName");

    public final NumberPath<Double> cnyRate = createNumber("cnyRate", Double.class);

    public final StringPath coldWalletAddress = createString("coldWalletAddress");

    public final NumberPath<Integer> decimals = createNumber("decimals", Integer.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> enableRpc = createEnum("enableRpc", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final BooleanPath hasLegal = createBoolean("hasLegal");

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> isPlatformCoin = createEnum("isPlatformCoin", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final EnumPath<ai.turbochain.ipex.constant.BooleanEnum> isToken = createEnum("isToken", ai.turbochain.ipex.constant.BooleanEnum.class);

    public final NumberPath<Double> maxTxFee = createNumber("maxTxFee", Double.class);

    public final NumberPath<java.math.BigDecimal> maxWithdrawAmount = createNumber("maxWithdrawAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> minerFee = createNumber("minerFee", java.math.BigDecimal.class);

    public final NumberPath<Double> minTxFee = createNumber("minTxFee", Double.class);

    public final NumberPath<java.math.BigDecimal> minWithdrawAmount = createNumber("minWithdrawAmount", java.math.BigDecimal.class);

    public final StringPath name = createString("name");

    public final StringPath nameCn = createString("nameCn");

    public final NumberPath<Integer> sort = createNumber("sort", Integer.class);

    public final EnumPath<ai.turbochain.ipex.constant.CommonStatus> status = createEnum("status", ai.turbochain.ipex.constant.CommonStatus.class);

    public final StringPath tokenAddress = createString("tokenAddress");

    public final StringPath unit = createString("unit");

    public final NumberPath<Double> usdRate = createNumber("usdRate", Double.class);

    public final NumberPath<Integer> withdrawScale = createNumber("withdrawScale", Integer.class);

    public final NumberPath<java.math.BigDecimal> withdrawThreshold = createNumber("withdrawThreshold", java.math.BigDecimal.class);

    public QCoin(String variable) {
        super(Coin.class, forVariable(variable));
    }

    public QCoin(Path<? extends Coin> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCoin(PathMetadata metadata) {
        super(Coin.class, metadata);
    }

}

