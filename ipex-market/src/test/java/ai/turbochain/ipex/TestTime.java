package ai.turbochain.ipex;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import ai.turbochain.ipex.util.DateUtil;


public class TestTime {
    public static final DateFormat YYYY_MM_DD_MM_HH_SS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static void main(String[] args) {
	 
		
		//小时//1561688581000===1566872581515
		//2019-06-28 10:23:01 2019-08-27 10:23:01
		
	//天	//1535765908000===1566869908759
		//2018-09-01 09:38:28 2019-08-27 09:38:28
		//1528508325000===1535765907000
		//2018-06-09 09:38:45 2018-09-01 09:38:27
		
	//周 1411350059000===1566870059303
		//2014-09-22 09:40:59 2019-08-27 09:40:59
		//1347241305000===1411350058000
		//2012-09-10 09:41:45 2014-09-22 09:40:58
		
	//月633750193000===1566870193857
		//1990-01-31 09:43:13 2019-08-27 09:43:13
		//356406197000===633750192000
		//1981-04-18 09:43:17 1990-01-31 09:43:12
		//long time = 1567267200000l; //2019-09-01 00:00:00 8069.3400
		long time =  1569168000000l; //2019-09-23 00:00:00 :8069.3400
		 time =  1569772800000l;//2019-09-30 00:00:00 8294.67
		 time =1570377600000l ; //2019-10-07 00:00:00
		 
	//	 time = 1582007286005l;//2020-02-18 14:28:06
	//	 time = 1582180086005l;//2020-02-20 14:28:06
		 time = 1582007340000l; //2020-02-18 14:29:00
		 time = 1582007400000l;//2020-02-18 14:30:00
		 
		 time = 1582180020000l;//2020-02-20 14:27:00
		 //time =  1582180080000l;//2020-02-20 14:28:00
		 time = 1582128000000l;//2020-02-20 00:00:00
		 time =  1582387200000l;//2020-02-23 00:00:00	
		 
		Date date = new Date(time);
		int currentPage =1;
		Integer s =1;
		//while (currentPage==s) {
			System.out.println("====="+YYYY_MM_DD_MM_HH_SS.format(date));
        //}  
        
		//	final long startTick = DateUtil.getYestDayBeginTime();
	    //	final long endTick =  DateUtil.getTodayBeginTime();
			
		//	System.out.println(startTick + "========" + endTick);
	}
}
