package ai.turbochain.ipex.controller.screen;

import ai.turbochain.ipex.constant.LegalWalletState;
import lombok.Data;

/**
 * @author jack
 * @Title: 
 * @Description:
 * @date 2020/4/217:44
 */
@Data
public class LegalWalletScreen {
    private LegalWalletState state;
    private String coinName;
}
